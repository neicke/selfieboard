import { Injectable } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

export interface Post {
    imageUrl: string;
    accessKey: string;
    uploadTime: Date;
}

@Injectable()
export class DisplayService {
    getPostsUrl = 'https://selfieboard-backend-211009.appspot.com/latest';
    getPostsSinceUrl = 'https://selfieboard-backend-211009.appspot.com/since';
    allPosts = new Array();

    constructor(private http: HttpClient) { }

    getLatestPost(): Observable<Post[]> {
        return this.http.get<Post[]>(this.getPostsUrl)
            .pipe(
                tap(heroes => this.log(`fetched heroes`)),
                catchError(this.handleError('getLatest', []))
            )
    }

    getPostsSince(lastPost): Observable<Post[]> {
        let params = new HttpParams().set('since', lastPost);

        return this.http.get<Post[]>(this.getPostsSinceUrl, { params: params })
            .pipe(
                catchError(this.handleError('getPostsSince', []))
            );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a HeroService message with the MessageService */
    private log(message: string) {
        console.log('DisplyService: ' + message);
    }
}