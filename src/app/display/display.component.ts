import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {Post} from './display.service';
import {DisplayService } from './display.service';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css'],
  providers: [ DisplayService ]
})
export class DisplayComponent implements OnInit {
  posts: Post[];
  activePost: Post;
  displayUrl = '';
  width: string;

  constructor(private displayService: DisplayService) { }

  ngOnInit() {
    this.queryServer.bind(this);
    window.setInterval(this.queryServer.bind(this), 1000);
    this.width = '40%';
  }

  queryServer() {

    if(this.activePost === undefined){
      var posts = this.displayService.getLatestPost().subscribe(posts => {
        this.posts = posts;
      });
    }
    this.activePost=this.posts[0];
    this.displayUrl = this.activePost.imageUrl;

    if(this.activePost !== undefined){
      this.displayService.getPostsSince(this.activePost.uploadTime).subscribe(posts => {
        console.log("posts since "+ this.activePost.uploadTime + ": " + posts.length);
        this.activePost = posts[0];
      });
    }
    this.displayUrl = this.posts[0].imageUrl;

    var img = new Image();
    var self = this;
    img.addEventListener("load", function(){
      if( this.naturalWidth < this.naturalHeight ){
        self.width='40%';
      } else {
        self.width='100%';
      }
    });
    img.src = this.displayUrl;
  }
  
}