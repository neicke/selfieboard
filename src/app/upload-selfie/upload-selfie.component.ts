import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { all } from 'q';

enum Step {
  selectFile,
  fileSelected,
  uploadInProgress,
  uploadCompletedShowCheckmark,
  uploadCompleted
}

@Component({
  selector: 'app-upload-selfie',
  templateUrl: './upload-selfie.component.html',
  styleUrls: ['./upload-selfie.component.css']
})
export class UploadSelfieComponent implements OnInit {

  // brightness(2.1) contrast(0.6) actualOpacity(0.2)
  brightness = 2.1;
  contrast = 0.6;
  opacity = 0.2;

  imageClass = null;
  selectedFile: File = null;
  previewUrl = "javascript:void(0)";

  steps = Step;
  currentStep: Step = Step.selectFile;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.previewUrl = event.target.result;
    }
    reader.readAsDataURL(this.selectedFile);

    this.currentStep = Step.uploadInProgress;

    this.onUpload();
  }

  onUpload() {
    console.log("Should upload " + this.selectedFile.name);

    const formData = new FormData();
    formData.append('usrfile', this.selectedFile, this.selectedFile.name);
    this.http.post('https://limitless-tundra-75551.herokuapp.com/upload', formData, { reportProgress: true, observe: "events" })
      .subscribe(event => {

        if (event.type === HttpEventType.UploadProgress) {
          this.currentStep = Step.uploadInProgress;
          this.opacity = 0.2 + 0.8 * (event.loaded / event.total);
          console.log(this.opacity);
        }

        if (event.type === HttpEventType.Response) {
          this.currentStep = Step.uploadCompletedShowCheckmark;

          this.brightness = 0;
          this.contrast = 1;
          this.opacity = 1;
        }
      });
  }

}

function getDifference(num1, num2) {
  return (num1 > num2) ? num1 - num2 : num2 - num1
}
