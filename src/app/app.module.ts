import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UploadSelfieComponent } from './upload-selfie/upload-selfie.component';
import { DisplayComponent } from './display/display.component';
import { PhotoboxComponent } from './photobox/photobox.component';
import {WebcamModule} from 'ngx-webcam';

const appRoutes: Routes = [
  { path: 'display', component: DisplayComponent },
  { path: 'photobox', component: PhotoboxComponent },
  { path: '**', component: UploadSelfieComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UploadSelfieComponent,
    DisplayComponent,
    PhotoboxComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    WebcamModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
